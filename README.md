# 231 - Persönliches Portfolio von Max Müller

*Diese Markdown-Datei bearbeiten und Namen anpassen!*

# Inhaltsverzeichnis
 - [Lernjournal](/01_Lernjournal/)
 - [Meine Sicherheit](02_Sicherheit/)
 - [Passwortverwaltung](03_Passwortverwaltung/)
 - [Ablage- und Backupkonzept](04_Ablage-%20und%20Backupkonzept/)
